from math import ceil
from unittest import TestCase

from kivy.vector import Vector

from roguelike.grid import Grid, Cell


class GridSmallTest(TestCase):
    TEST_CELL_WIDTH = 5
    TEST_CELL_HEIGHT = 4
    TEST_CELL_HALF_WIDTH = TEST_CELL_WIDTH / 2
    TEST_CELL_HALF_HEIGHT = TEST_CELL_HEIGHT / 2

    def setUp(self):
        self.test_obj = Grid(self.TEST_CELL_WIDTH, self.TEST_CELL_HEIGHT)

    def test_get_unset_cell_returns_empty_cell(self):
        cell = self.test_obj.get_cell(0, 0)
        assert cell.is_empty

    def test_get_previously_set_cell_returns_value(self):
        x, y = (0, 0)
        cell_content = object()
        self.test_obj.set_cell(x, y, cell_content)
        result_cell = self.test_obj.get_cell(x, y)
        assert result_cell.content == cell_content

    def test_get_previously_set_cell_returns_cell_with_bounds(self):
        x, y = (2, -3)
        cell_content = object()
        self.test_obj.set_cell(x, y, cell_content)
        result_cell = self.test_obj.get_cell(x, y)
        print(result_cell.top_left, result_cell.bottom_right)
        assert result_cell.top == -3 * self.TEST_CELL_HEIGHT + self.TEST_CELL_HALF_HEIGHT
        assert result_cell.left == 2 * self.TEST_CELL_WIDTH - self.TEST_CELL_HALF_WIDTH
        assert result_cell.bottom == -3 * self.TEST_CELL_HEIGHT - self.TEST_CELL_HALF_HEIGHT
        assert result_cell.right == 2 * self.TEST_CELL_WIDTH + self.TEST_CELL_HALF_WIDTH

    def test_get_previously_cleared_cell_returns_empty_cell(self):
        x, y = (0, 0)
        self.test_obj.set_cell(x, y, object())
        self.test_obj.clear_cell(x, y)
        result_cell = self.test_obj.get_cell(x, y)
        assert result_cell.is_empty

    def test_get_cell_at_returns_same_value_when_position_inside_set_cell_bounds(self):
        top, left = (ceil(self.TEST_CELL_HALF_HEIGHT), ceil(-self.TEST_CELL_HALF_WIDTH))
        bottom, right = (ceil(-self.TEST_CELL_HALF_HEIGHT), ceil(self.TEST_CELL_HALF_WIDTH))

        cell_content = object()
        self.test_obj.set_cell(0, 0, cell_content)

        for x in range(left, right):
            for y in range(top, bottom):
                assert self.test_obj.get_cell_at(Vector(x, y)).content == cell_content

    def test_get_cell_at_returns_empty_cell_when_position_outside_set_cell_bounds(self):
        top, left = (ceil(self.TEST_CELL_HALF_HEIGHT)+1, ceil(-self.TEST_CELL_HALF_WIDTH)-1)
        bottom, right = (ceil(-self.TEST_CELL_HALF_HEIGHT)-1, ceil(self.TEST_CELL_HALF_WIDTH)+1)

        self.test_obj.set_cell(0, 0, object())

        for x in [left, right]:
            for y in range(top, bottom):
                assert self.test_obj.get_cell_at(Vector(x, y)).is_empty

        for y in [top, bottom]:
            for x in range(left, right):
                assert self.test_obj.get_cell_at(Vector(x, y)).is_empty

    def test_get_cell_at_returns_empty_cell_when_unset(self):
        cell = self.test_obj.get_cell_at(Vector(-15, 25))
        assert cell.is_empty

    def test_get_cell_xy_returns_cell_xy_coord(self):
        x, y = self.test_obj.get_cell_xy(7, -2)
        assert x == 1
        assert y == 0

    def test_each_cell_iterates_through_all_previously_set_cells(self):
        iterated_cells = 0
        set_cells = [(0, 0), (-283, -179), (-3, 2), (4, -8), (871, 901)]

        for cell in set_cells:
            self.test_obj.set_cell(cell[0], cell[1], object())

        for position, cell in self.test_obj.each_cell():
            assert position in set_cells
            assert not cell.is_empty
            iterated_cells += 1

        assert iterated_cells == len(set_cells)

    def test_each_cell_in_iterates_through_all_previously_set_cells_inside_bounds_by_default(self):
        iterated_cells = 0
        set_cells = [(0, 0), (-5, -1), (-3, 2), (5, -9), (1, 7)]

        for cell in set_cells:
            self.test_obj.set_cell(cell[0], cell[1], object())

        for position, cell in self.test_obj.each_cell_in(-5, -9, 6, 8):
            assert position in set_cells
            assert not cell.is_empty
            iterated_cells += 1

        assert iterated_cells == len(set_cells)

    def test_each_cell_in_iterates_through_all_cells_inside_bounds_required(self):
        iterated_cells = 0

        for position, cell in self.test_obj.each_cell_in(-1, -1, 2, 2, True):
            iterated_cells += 1

        assert iterated_cells == 9

    def test_each_cell_in_left_top_bounds_included_right_bottom_excluded(self):
        iterated_cells = 0
        set_cells = [(0, 0), (-6, -3)]

        for cell in set_cells:
            self.test_obj.set_cell(cell[0], cell[1], object())

        for position, cell in self.test_obj.each_cell_in(-6, -3, 0, 0):
            assert position in set_cells
            assert not cell.is_empty
            iterated_cells += 1

        assert iterated_cells == 1
