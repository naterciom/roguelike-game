from kivy.uix.widget import Widget
from kivy.vector import Vector


class Component(object):
    
    def update(self):
        raise NotImplementedError


class Actor(Widget):
    
    def __init__(self, **kwargs):
        Widget.__init__(self, **kwargs)
        self.components = []

    def update(self, delta_time: float):
        for c in self.components:
            c.update()

    @property
    def position(self):
        return Vector(self.pos[0], self.pos[1])

    @position.setter
    def position(self, value: Vector):
        self.pos = (value.x, value.y)

    def add_component(self, component: Component):
        self.components.append(component)


class BaseComponent(Component):

    def __init__(self, owner: Actor):
        owner.add_component(self)
        self._owner = owner

    @property
    def owner(self) -> Actor:
        return self._owner


class DummyComponent(BaseComponent):

    def __init__(self, owner):
        BaseComponent.__init__(self, owner)

    def update(self):
        pass

