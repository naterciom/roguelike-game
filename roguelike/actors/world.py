from typing import Tuple

from actors import Actor, BaseComponent
from grid import Grid


class TileMap(BaseComponent):

    def __init__(self, owner: Actor, grid: Grid):
        BaseComponent.__init__(self, owner)

        self._grid = grid

    def update(self):
        pass


class World(Actor):

    def __init__(self, grid: Grid, **kwargs):
        Actor.__init__(self, **kwargs)

        self._grid = grid

        TileMap(self, grid)
