from random import randint

from kivy.graphics.vertex_instructions import Ellipse, Rectangle
from kivy.graphics.context_instructions import Color
from kivy.graphics.texture import Texture
from kivy.vector import Vector
from kivy.uix.image import Image

from actors import Actor, DummyComponent
from actors.controllers import Controllable, InputController
from grid import Grid, GridControllable


class Player(Actor):

    texture = Image(source='atlas://assets/char/down-1').texture

    def __init__(self, grid: Grid, **kwargs):
        Actor.__init__(self, **kwargs)
        self.size_hint = (None, None)
        self.size = (32, 32)
        self.pos = Vector(randint(0, 500), randint(0, 300))

        with self.canvas:
            self.body = Rectangle(texture=Player.texture,
                                  size=Player.texture.size,
                                  pos=self.pos)

        self.bind(pos=self._update_pos)

        GridControllable(self, grid, controller=InputController())

    def _update_pos(self, a, b):
        self.body.pos = self.pos


class Dummy(Actor):

    texture = Image(source='atlas://assets/char/up-0').texture

    def __init__(self, **kwargs):
        Actor.__init__(self, **kwargs)
        self.size_hint = (None, None)
        self.size = (32, 32)
        self.pos = (randint(0, 500), randint(0, 300))

        with self.canvas:
            self.body = Rectangle(texture=Dummy.texture, size=Dummy.texture.size, pos=self.pos)

        DummyComponent(self)
