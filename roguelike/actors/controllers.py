from kivy.core.window import Window, Keyboard
from kivy.vector import Vector

from actors import BaseComponent, Actor


class Controller(object):

    def axis(self) -> Vector:
        raise NotImplementedError


class Controllable(BaseComponent):

    def __init__(self, owner: Actor, controller: Controller):
        BaseComponent.__init__(self, owner)

        self._controller = controller

    def update(self):
        axis = self._controller.axis()
        self.owner.pos = axis + self.owner.pos


class InputController(Controller):

    _directions = {
        'up': Vector(0, 1),
        'down': Vector(0, -1),
        'right': Vector(1, 0),
        'left': Vector(-1, 0)
    }

    def __init__(self):
        self._axis = Vector(0, 0)

        self._keys = set()
        self._keyboard = Window.request_keyboard(None, self)
        self._keyboard.bind(on_key_down=self._key_down_handler)
        self._keyboard.bind(on_key_up=self._key_up_handler)

    def _key_down_handler(self, key, scancode=None, codepoint=None, modifier=None, **kwargs):
        if not scancode[1] in self._keys:
            try:
                self._axis = InputController._directions[scancode[1]] + self._axis
                self._keys.add(scancode[1])
            finally:
                return True

        return False

    def _key_up_handler(self, key, scancode=None, codepoint=None, modifier=None, **kwargs):
        if scancode[1] in self._keys:
            try:
                self._axis = InputController._directions[scancode[1]] * -1 + self._axis
                self._keys.discard(scancode[1])
            finally:
                return True

        return False

    def axis(self) -> Vector:
        return self._axis
