from math import floor
from typing import Tuple, Generator

from kivy.vector import Vector

from actors import Actor
from actors.controllers import Controllable


class Cell(object):

    def __init__(self, top: int, left: int, width: int, height: int, content: object = None):
        self._content = content

        self._top = top
        self._left = left
        self._bottom = top - height
        self._right = left + width
        self._width = width
        self._height = height

    @property
    def content(self) -> object:
        return self._content

    @content.setter
    def content(self, value: object):
        self._content = value

    @property
    def top(self) -> int:
        return self._top

    @property
    def left(self) -> int:
        return self._left

    @property
    def top_left(self) -> Tuple[int, int]:
        return self.top, self.left

    @property
    def bottom(self) -> int:
        return self._bottom

    @property
    def right(self) -> int:
        return self._right

    @property
    def bottom_right(self) -> Tuple[int, int]:
        return self.bottom, self.right

    @property
    def center(self) -> Tuple[int, int]:
        return int(self.left + self._width / 2), int(self.top - self._height / 2)

    @property
    def is_empty(self) -> bool:
        return self._content is None


class Grid(object):

    def __init__(self, cell_width: int, cell_height: int):
        self._cell_width = cell_width
        self._cell_height = cell_height
        self._offset = Vector(cell_width / 2, cell_height / 2)
        self._cells = {}

    def set_cell(self, x: int, y: int, content: object):
        new_cell = self._empty_cell(x, y)
        new_cell.content = content
        self._cells[x, y] = new_cell

    def set_cell_at(self, position: Vector, content: object):
        x, y = self.get_cell_xy(position)
        self.set_cell(x, y, content)

    def get_cell(self, x: int, y: int) -> Cell:
        return self._cells[x, y] if (x, y) in self._cells else self._empty_cell(x, y)

    def get_cell_at(self, position: Vector) -> Cell:
        x, y = self.get_cell_xy(position)
        return self.get_cell(x, y)

    def get_cell_xy(self, position: Vector) -> Tuple[int, int]:
        return floor((position.x + self._offset.x) / self._cell_width), floor((position.y + self._offset.y) / self._cell_height)

    def clear_cell(self, x: int, y: int):
        del self._cells[x, y]

    def each_cell(self):
        return self._cells.items()

    def each_cell_in(self, left: int, top: int, right: int, bottom: int, include_unset: bool = False) -> Generator[Tuple[Tuple[int, int], object], None, None]:
        for x in range(left, right):
            for y in range(top, bottom):
                cell = self.get_cell(x, y)
                if not cell.is_empty or include_unset:
                    yield (x, y), cell

    def _empty_cell(self, x: int, y: int) -> Cell:
        return Cell(y * self._cell_height + self._offset.y,
                    x * self._cell_width - self._offset.x,
                    self._cell_width,
                    self._cell_height)


class GridControllable(Controllable):

    def __init__(self, owner: Actor, grid: Grid, **kwargs):
        Controllable.__init__(self, owner, **kwargs)

        self._grid = grid

        self._last_axis = None

    def update(self):
        axis = self._controller.axis()

        if axis != self._last_axis:
            cell_xy = Vector(self._grid.get_cell_xy(self.owner.position))
            new_cell_xy = cell_xy + axis
            self.owner.position = Vector(self._grid.get_cell(new_cell_xy.x, new_cell_xy.y).center)
            self._last_axis = axis
