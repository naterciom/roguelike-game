from kivy.app import App
from kivy.config import Config
from kivy.uix.widget import Widget
from kivy.clock import Clock

from actors.player import Player, Dummy
from grid import Grid
from actors.world import World

MAX_FPS = 100


class RoguelikeGame(Widget):

    def start(self):
        self.actors = []

        grid = Grid(50, 50)

        world = World(grid)
        self.add_actor(world)

        player = Player(grid)
        self.add_actor(player)

    def add_actor(self, actor):
        self.actors.append(actor)
        self.add_widget(actor)

    def update(self, delta_time):
        for a in self.actors:
            a.update(delta_time)


class RoguelikeApp(App):

    def build(self):
        game = RoguelikeGame()
        game.start()
        Clock.schedule_interval(game.update, 1.0 / MAX_FPS)
        return game


if __name__ == '__main__':
    Config.set('graphics', 'borderless', 1)
    Config.set('modules', 'monitor', '')

    RoguelikeApp().run()
